//
//  PlayerSelectionViewController.swift
//  Tap First
//
//  Created by brst on 27/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation

class PlayerSelectionViewController: UIViewController
{
    // MARK: - Variables Declaration
    var audioPlayer:AVAudioPlayer!
    var playerSelectionString = String()
    var firebaseGroupsArray = NSMutableArray()
    var childRef = FIRDatabase.database().reference()
    var userID_saved = String()
    var userName_saved = String()
    var userToken_saved = String()
    var userStatus_saved = String()
    var usersArr = NSDictionary()
     var getNetworkStatus = String()
    
    // MARK: - Outlets Connection
    @IBOutlet var playerTitle_lbl: UILabel!
    @IBOutlet var selectPlayerHeading_lbl: UILabel!
    @IBOutlet var loadingView: UIView!
    @IBOutlet var playerSelectionInnerView: UIView!
    @IBOutlet var noOfPlayer_lbl: UILabel!
    @IBOutlet var selectplayer_lbl: UILabel!
    @IBOutlet var viewReportLbl: UILabel!
    @IBOutlet var viewReportBtn: UIButton!
    @IBOutlet var viewReport_imageView: UIImageView!
    @IBOutlet var viewReportView: UIView!
    @IBOutlet var playerSelectionBtn: UIButton!
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var playerView: UIView!
    @IBOutlet var playerOneBtn: UIButton!
    @IBOutlet var playerTwoBtn: UIButton!
    @IBOutlet var playerThreeBtn: UIButton!
    @IBOutlet var playerFourBtn: UIButton!
    @IBOutlet var playerFiveBtn: UIButton!
    @IBOutlet var playerSelectionOnTextBtn: UIButton!
    
    
    // MARK: - View-Did-Load Method
    override func viewDidLoad()
    {
        super.viewDidLoad()

        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        
        UserDefaults.standard.set(("Online" as AnyObject), forKey: "Status")

        if UserDefaults.standard.value(forKey: "userName") != nil
        {
            print("saved name of the player itself = \(UserDefaults.standard.value(forKey: "userName")!)")
            
            userName_saved = UserDefaults.standard.value(forKey: "userName") as! String
        }
        if  UserDefaults.standard.value(forKey: "UserId") != nil
        {
            print("saved id of the player itself = \(UserDefaults.standard.value(forKey: "UserId")!)")
            userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
        }
        
        if UserDefaults.standard.value(forKey: "Status") != nil
        {
            print("saved Status of the player itself = \(UserDefaults.standard.value(forKey: "Status")!)")
            userStatus_saved = UserDefaults.standard.value(forKey: "Status") as! String
        }
        
        if UserDefaults.standard.value(forKey: "Token") != nil
        {
            print("saved Token of the player itself = \(UserDefaults.standard.value(forKey: "Token")!)")
            userToken_saved = UserDefaults.standard.value(forKey: "Token") as! String
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(PlayerSelectionViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reach().monitorReachabilityChanges()

    }
    
    
    // MARK: - Get Network Status Reachibility Method
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        print("userInfo for network reachibility = \(userInfo)")
        
        getNetworkStatus = (userInfo?["Status"] as? String)!
        print("userInfo for network reachibility in string = \(getNetworkStatus)")
        
        if getNetworkStatus == "Offline"
        {
            
            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    

    // MARK: - View-Will-Appear Method
    override func viewWillAppear(_ animated: Bool)
    {
        playerView.isHidden=true
        
//        PlaySoundInBackground()
    }
    
    
    
    
    
    // MARK: - Play Audio File In Background Method
    func PlaySoundInBackground()
    {
        let audioFilePath = Bundle.main.path(forResource: "Playscreen, enter username screen, choose how many players screen and Match report screen. ", ofType: "mp3")
       let url = URL(fileURLWithPath: audioFilePath!)
        
        do
        {
            let sound = try AVAudioPlayer(contentsOf: url)
            audioPlayer = sound
            sound.play()
        }
        catch
        {
            // couldn't load file :(
            print("couldn't load file")
        }
        
    }
    
    
    
    // MARK: -  Player Selection Drop Down Btn Action
    @IBAction func playerSelect_dropDownBtn(_ sender: UIButton)
    {
        print("playerSelect_dropDownBtn")
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan):
            print("Connected via WWAN")
            playerView.isHidden = false
        case .online(.wiFi):
            print("Connected via WiFi")
            playerView.isHidden = false
        }
        
    }
    
    
    
    // MARK: -  Player Selection Btn Action
    @IBAction func playerSelectionBtn_Action(_ sender: UIButton)
    {
        print("playerSelectionBtn_Action = \(sender.tag)")
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            playerView.isHidden = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        case .online(.wwan), .online(.wiFi):
            print("Connected via WWAN/Connected via WiFi")
            
            let refreshedToken = FIRInstanceID.instanceID().token()
            print("InstanceID token: \(refreshedToken)")
            // 1
            let rootRef = FIRDatabase.database().reference()
            
            if sender.tag == 1
            {
                
                self.loadingView.isHidden = false
                
                playerSelectionString = "two_users"
                UserDefaults.standard.setValue(playerSelectionString, forKey: "playerSelectionString")
                
                noOfPlayer_lbl.text = "1"
                noOfPlayer_lbl.isHidden = false
                playerTitle_lbl.isHidden = false
                selectPlayerHeading_lbl.isHidden = true
                
                childRef.child("groups").child("two_users").observeSingleEvent(of: .value, with: { snapshot in
                    
                    print("snapshot.childrenCount = \(snapshot.childrenCount)")
                    
                    if snapshot.childrenCount  == 0
                    {
                        UserDefaults.standard.set(nil, forKey: "keyGenerated")
                        self.createNewGroup()
                    }
                    else
                    {
                        let enumerator = snapshot.children
                        
                        var   anyGroupToJoinFound = false
                        
                        
                        while let rest = enumerator.nextObject() as? FIRDataSnapshot
                        {
                            
                            print("rest.key = \(rest.key)")
                            print("rest.value = \(rest.value)")
                            
                            var dict1 = NSDictionary()
                            dict1 = rest.value! as! NSDictionary
                            print(dict1)
                            
                            if (dict1 .value(forKey:  "user_count") as! String)  == "1"
                            {
                                anyGroupToJoinFound = true
                                print("count is 1")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                self.childRef.child("groups").child("two_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("two_users").child(rest.key).child("user_count").setValue("2")
                                self.childRef.child("groups").child("two_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                        }
                        
                        if anyGroupToJoinFound == false
                        {
                            self.createNewGroup()
                        }
                        
                        
                    }
                })
            }
            else if sender.tag == 2
            {
                self.loadingView.isHidden = false
                playerSelectionString = "three_users"
                UserDefaults.standard.setValue(playerSelectionString, forKey: "playerSelectionString")
                noOfPlayer_lbl.text = "2"
                noOfPlayer_lbl.isHidden = false
                playerTitle_lbl.isHidden = false
                selectPlayerHeading_lbl.isHidden = true
                
                childRef.child("groups").child("three_users").observeSingleEvent(of: .value, with: { snapshot in
                    
                    print("snapshot.childrenCount = \(snapshot.childrenCount)")
                    
                    if snapshot.childrenCount  == 0
                    {
                        UserDefaults.standard.set(nil, forKey: "keyGenerated")
                        self.createNewGroup_threeUsers()
                    }
                    else
                    {
                        let enumerator = snapshot.children
                        
                        while let rest = enumerator.nextObject() as? FIRDataSnapshot
                        {
                            print("rest.key = \(rest.key)")
                            print("rest.value = \(rest.value)")
                            
                            var dict1 = NSDictionary()
                            dict1 = rest.value! as! NSDictionary
                            print(dict1)
                            
                            var anyGroupToJoinFound = false
                            
                            if (dict1 .value(forKey:  "user_count") as! String)  == "1"
                            {
                                anyGroupToJoinFound = true
                                print("count is 1")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("three_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("three_users").child(rest.key).child("user_count").setValue("2")
                                self.childRef.child("groups").child("three_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "2"
                            {
                                anyGroupToJoinFound = true
                                print("count is 1")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("three_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("three_users").child(rest.key).child("user_count").setValue("3")
                                self.childRef.child("groups").child("three_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            
                            
                            
                            if anyGroupToJoinFound == false
                            {
                                self.createNewGroup_threeUsers()
                            }
                            
                            
                        }
                        
                    }
                })
            }
            else if sender.tag == 3
            {
                self.loadingView.isHidden = false
                playerSelectionString = "four_users"
                UserDefaults.standard.setValue(playerSelectionString, forKey: "playerSelectionString")
                noOfPlayer_lbl.text = "3"
                noOfPlayer_lbl.isHidden = false
                playerTitle_lbl.isHidden = false
                selectPlayerHeading_lbl.isHidden = true
                
                childRef.child("groups").child("four_users").observeSingleEvent(of: .value, with: { snapshot in
                    
                    print("snapshot.childrenCount = \(snapshot.childrenCount)")
                    
                    if snapshot.childrenCount  == 0
                    {
                        UserDefaults.standard.set(nil, forKey: "keyGenerated")
                        self.createNewGroup_fourUsers()
                    }
                    else
                    {
                        let enumerator = snapshot.children
                        
                        while let rest = enumerator.nextObject() as? FIRDataSnapshot
                        {
                            print("rest.key = \(rest.key)")
                            print("rest.value = \(rest.value)")
                            
                            var dict1 = NSDictionary()
                            dict1 = rest.value! as! NSDictionary
                            print(dict1)
                            
                            var anyGroupToJoinFound = false
                            
                            if (dict1 .value(forKey:  "user_count") as! String)  == "1"
                            {
                                anyGroupToJoinFound = true
                                print("count is 1")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("four_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("four_users").child(rest.key).child("user_count").setValue("2")
                                self.childRef.child("groups").child("four_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "2"
                            {
                                anyGroupToJoinFound = true
                                print("count is 2")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("four_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("four_users").child(rest.key).child("user_count").setValue("3")
                                self.childRef.child("groups").child("four_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "3"
                            {
                                anyGroupToJoinFound = true
                                print("count is 3")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("four_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("four_users").child(rest.key).child("user_count").setValue("4")
                                self.childRef.child("groups").child("four_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            
                            
                            if anyGroupToJoinFound == false
                            {
                                self.createNewGroup_fourUsers()
                            }
                            
                            
                        }
                        
                    }
                })
                
            }
            else if sender.tag == 4
            {
                self.loadingView.isHidden = false
                playerSelectionString = "five_users"
                UserDefaults.standard.setValue(playerSelectionString, forKey: "playerSelectionString")
                noOfPlayer_lbl.text = "4"
                noOfPlayer_lbl.isHidden = false
                playerTitle_lbl.isHidden = false
                selectPlayerHeading_lbl.isHidden = true
                
                childRef.child("groups").child("five_users").observeSingleEvent(of: .value, with: { snapshot in
                    
                    print("snapshot.childrenCount = \(snapshot.childrenCount)")
                    
                    if snapshot.childrenCount  == 0
                    {
                        UserDefaults.standard.set(nil, forKey: "keyGenerated")
                        self.createNewGroup_fiveUsers()
                    }
                    else
                    {
                        let enumerator = snapshot.children
                        
                        while let rest = enumerator.nextObject() as? FIRDataSnapshot
                        {
                            print("rest.key = \(rest.key)")
                            print("rest.value = \(rest.value)")
                            
                            var dict1 = NSDictionary()
                            dict1 = rest.value! as! NSDictionary
                            print(dict1)
                            
                            var anyGroupToJoinFound = false
                            
                            if (dict1 .value(forKey:  "user_count") as! String)  == "1"
                            {
                                anyGroupToJoinFound = true
                                print("count is 1")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("five_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("user_count").setValue("2")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "2"
                            {
                                anyGroupToJoinFound = true
                                print("count is 2")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("five_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("user_count").setValue("3")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "3"
                            {
                                anyGroupToJoinFound = true
                                print("count is 3")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("five_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("user_count").setValue("4")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "4"
                            {
                                anyGroupToJoinFound = true
                                print("count is 3")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("five_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("user_count").setValue("5")
                                self.childRef.child("groups").child("five_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            
                            if anyGroupToJoinFound == false
                            {
                                self.createNewGroup_fiveUsers()
                            }
                            
                            
                        }
                        
                    }
                })
                
            }
            else
            {
                self.loadingView.isHidden = false
                playerSelectionString = "six_users"
                UserDefaults.standard.setValue(playerSelectionString, forKey: "playerSelectionString")
                noOfPlayer_lbl.text = "5"
                noOfPlayer_lbl.isHidden = false
                playerTitle_lbl.isHidden = false
                selectPlayerHeading_lbl.isHidden = true
                
                childRef.child("groups").child("six_users").observeSingleEvent(of: .value, with: { snapshot in
                    
                    print("snapshot.childrenCount = \(snapshot.childrenCount)")
                    
                    if snapshot.childrenCount  == 0
                    {
                        UserDefaults.standard.set(nil, forKey: "keyGenerated")
                        self.createNewGroup_SixUsers()
                    }
                    else
                    {
                        let enumerator = snapshot.children
                        
                        while let rest = enumerator.nextObject() as? FIRDataSnapshot
                        {
                            print("rest.key = \(rest.key)")
                            print("rest.value = \(rest.value)")
                            
                            var dict1 = NSDictionary()
                            dict1 = rest.value! as! NSDictionary
                            print(dict1)
                            
                            var anyGroupToJoinFound = false
                            
                            if (dict1 .value(forKey:  "user_count") as! String)  == "1"
                            {
                                anyGroupToJoinFound = true
                                print("count is 1")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("six_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("user_count").setValue("2")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "2"
                            {
                                anyGroupToJoinFound = true
                                print("count is 2")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("six_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("user_count").setValue("3")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "3"
                            {
                                anyGroupToJoinFound = true
                                print("count is 3")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("six_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("user_count").setValue("4")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "4"
                            {
                                anyGroupToJoinFound = true
                                print("count is 3")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("six_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("user_count").setValue("5")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            else  if (dict1 .value(forKey:  "user_count") as! String)  == "5"
                            {
                                anyGroupToJoinFound = true
                                print("count is 3")
                                
                                self.userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
                                
                                let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
                                
                                self.childRef.child("groups").child("six_users").child(rest.key).child("points").setValue("Empty")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("user_count").setValue("6")
                                self.childRef.child("groups").child("six_users").child(rest.key).child("users").child(self.userID_saved).setValue( aStr )
                                
                                UserDefaults.standard.set(rest.key, forKey: "keyGenerated")
                                UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
                                self.childRef.removeAllObservers()
                                self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
                                break;
                            }
                            
                            if anyGroupToJoinFound == false
                            {
                                self.createNewGroup_SixUsers()
                            }
                            
                            
                        }
                        
                    }
                })
                
            }
            
            playerView.isHidden = true
            
            //        case .online(.wiFi):
            //            print("Connected via WiFi")
        }
        
    }
    
    

    
    // MARK: -  Create New Group For Two Users
    func createNewGroup()
    {
        let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
        
        // save user into the firebase realtime database
        let keyGenerated = self.childRef.child("groups").childByAutoId().key
        
        print("key generated during new group = \(keyGenerated)")
        
        // save key generated in UserDefaults
        UserDefaults.standard.setValue(keyGenerated, forKey: "NewGroupKeyGenerated")
        print("NewGroupKeyGenerated generated during new group = \(UserDefaults.standard.value(forKey: "NewGroupKeyGenerated"))")
        
        UserDefaults.standard.set(true, forKey: "twoUserGroupCheck")
        UserDefaults.standard.set(keyGenerated as String, forKey: "keyGenerated")
        UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
        
       self.childRef.child("groups").child("two_users").child(keyGenerated).child("points").setValue("Empty")
       self.childRef.child("groups").child("two_users").child(keyGenerated).child("user_count").setValue("1")
       self.childRef.child("groups").child("two_users").child(keyGenerated).child("users").child(self.userID_saved).setValue(aStr)
        
        self.loadingView.isHidden = true
        
        self.childRef.removeAllObservers()
        
        self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
    }
    
    
    
    // MARK: -  Create New Group For Three Users
    func createNewGroup_threeUsers()
    {
        let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
        
        // save user into the firebase realtime database
        let keyGenerated = self.childRef.child("groups").childByAutoId().key
        print("key generated during new group = \(keyGenerated)")
        
//        UserDefaults.standard.set(true, forKey: "twoUserGroupCheck")
        UserDefaults.standard.set((keyGenerated as AnyObject), forKey: "keyGenerated")
        UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
        
        self.childRef.child("groups").child("three_users").child(keyGenerated).child("points").setValue("Empty")
        self.childRef.child("groups").child("three_users").child(keyGenerated).child("user_count").setValue("1")
        self.childRef.child("groups").child("three_users").child(keyGenerated).child("users").child(self.userID_saved).setValue(aStr)
        
        self.loadingView.isHidden = true
        
        self.childRef.removeAllObservers()
        
        self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
    }

    
    
    // MARK: -  Create New Group For Four Users
    func createNewGroup_fourUsers()
    {
        let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
        
        // save user into the firebase realtime database
        let keyGenerated = self.childRef.child("groups").childByAutoId().key
        print("key generated during new group = \(keyGenerated)")
        
//        UserDefaults.standard.set(true, forKey: "twoUserGroupCheck")
        UserDefaults.standard.set((keyGenerated as AnyObject), forKey: "keyGenerated")
        UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
        
       self.childRef.child("groups").child("four_users").child(keyGenerated).child("points").setValue("Empty")
       self.childRef.child("groups").child("four_users").child(keyGenerated).child("user_count").setValue("1")
       self.childRef.child("groups").child("four_users").child(keyGenerated).child("users").child(self.userID_saved).setValue(aStr)
        
        self.loadingView.isHidden = true
        
        self.childRef.removeAllObservers()
        
        self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
    }

    
    
    
    
    // MARK: -  Create New Group For Five Users
    func createNewGroup_fiveUsers()
    {
        let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
        
        // save user into the firebase realtime database
        let keyGenerated = self.childRef.child("groups").childByAutoId().key
        print("key generated during new group = \(keyGenerated)")
        
        //        UserDefaults.standard.set(true, forKey: "twoUserGroupCheck")
        UserDefaults.standard.set((keyGenerated as AnyObject), forKey: "keyGenerated")
        UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
        
        self.childRef.child("groups").child("five_users").child(keyGenerated).child("points").setValue("Empty")
        self.childRef.child("groups").child("five_users").child(keyGenerated).child("user_count").setValue("1")
        self.childRef.child("groups").child("five_users").child(keyGenerated).child("users").child(self.userID_saved).setValue(aStr)
        
        self.loadingView.isHidden = true
        
        self.childRef.removeAllObservers()
        
        self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
    }
    
    
    
    
    
    // MARK: -  Create New Group For Six Users
    func createNewGroup_SixUsers()
    {
        let aStr = String(format: "%@:%d status %@", self.userName_saved,0,"Online")
        
        // save user into the firebase realtime database
        let keyGenerated = self.childRef.child("groups").childByAutoId().key
        print("key generated during new group = \(keyGenerated)")
        
        //        UserDefaults.standard.set(true, forKey: "twoUserGroupCheck")
        UserDefaults.standard.set((keyGenerated as AnyObject), forKey: "keyGenerated")
        UserDefaults.standard.set(true, forKey: "WaitingForOpponent")
        
        self.childRef.child("groups").child("six_users").child(keyGenerated).child("points").setValue("Empty")
        self.childRef.child("groups").child("six_users").child(keyGenerated).child("user_count").setValue("1")
        self.childRef.child("groups").child("six_users").child(keyGenerated).child("users").child(self.userID_saved).setValue(aStr)
        
        self.loadingView.isHidden = true
        
        self.childRef.removeAllObservers()
        
        self.perform(#selector(self.pushMethod), with: nil, afterDelay: 0.4)
    }
    
    
    
    
    // MARK: -  Move to Next Screen for Count-Down
    func pushMethod()
    {
        let countDownScreen = self.storyboard?.instantiateViewController(withIdentifier: "CountDownViewController") as! CountDownViewController
        countDownScreen.playerSelectionString = self.playerSelectionString
        self.navigationController?.pushViewController(countDownScreen, animated: true)
    }
    
    
    
    // MARK: -  View Report Histort Btn Action
    @IBAction func viewReportBtn_Action(_ sender: Any)
    {
        print("viewReportBtn_Action")
        let viewReportScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewReportViewController") as! ViewReportViewController
        self.navigationController?.pushViewController(viewReportScreen, animated: true)
    }
    

    
    // MARK: -  Back Btn Action
    @IBAction func backBtn_Action(_ sender: Any)
    {
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKind(of: PlayGameViewController.self)
            {
                self.navigationController?.popToViewController(controller , animated: true)
                break
            }
        }
    }
    
    
    // MARK: -  UITouch Began Method
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let touch = touches.first
        {
            playerView.isHidden=true
            noOfPlayer_lbl.isHidden = true
            playerTitle_lbl.isHidden = true
            selectPlayerHeading_lbl.isHidden = false
        }
        super.touchesBegan(touches, with: event)
    }
    
    
    // MARK: - Did-Receive-Memory-Warning Method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
