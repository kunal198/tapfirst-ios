//
//  AppDelegate.swift
//  Tap First
//
//  Created by brst on 27/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import GoogleMobileAds
import FirebaseDatabase
import Firebase
import AVFoundation

// MARK: - Global Variables Declaration
var PlayBtnClick_Ads = Int()
var audioPlayer:AVAudioPlayer!
var gameCompletionAds_bool = Bool()
var gameQuitAds_bool = Bool()
var moveToPlayingScreenBool = Bool()
var backgroundBoolForCountDown = Bool()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    // MARK: - Appdelegate Local Variables Declaration
    var moveUserToHome = Bool()
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    var window: UIWindow?
    var checkStatusBool = Bool()
    var audioOnPauseBool = Bool()
    
    
    // MARK: - Did-Finish-Launching-With-Options Method
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        
        checkStatusBool = false
        audioOnPauseBool = false
        gameCompletionAds_bool = false
        gameQuitAds_bool = false
        moveToPlayingScreenBool = false
        backgroundBoolForCountDown = false
        
         UserDefaults.standard.set(false, forKey: "WaitingForOpponent")
        
         print("PlayBtnClick_Ads  in beginning = \(PlayBtnClick_Ads)")
        
         print("NewGroupKeyGenerated generated during new group = \(UserDefaults.standard.value(forKey: "NewGroupKeyGenerated"))")
        
        if UserDefaults.standard.bool(forKey: "twoUserGroupCheck") == true
        {
            print("twoUserGroupCheck...waiting for user second")
        }
        else
        {
            print("twoUserGroupCheck... user second also entered")
        }
        
        

          UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
          FIRApp.configure()
         GADMobileAds.configure(withApplicationID: "ca-app-pub-8501671653071605~9497926137")
        
        
        if (UserDefaults.standard.value(forKey: "isTutorialScreen") != nil)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "PlayGameViewController") as! PlayGameViewController
            let navigationController = self.window?.rootViewController as! UINavigationController
            navigationController.pushViewController(walkthroughVC, animated: false)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let walkthroughVC = storyboard.instantiateViewController(withIdentifier: "InstructionsViewController") as! InstructionsViewController
            let navigationController = self.window?.rootViewController as! UINavigationController
            navigationController.pushViewController(walkthroughVC, animated: false)
        }
        
        return true
    }

    
    // MARK: - Application Will Resign Active Method
    func applicationWillResignActive(_ application: UIApplication)
    {
        self.doBackgroundTask()
        print("resignedddddddd")
    }

    
    // MARK: - Method for updating Background tasks and terminate application
    func doBackgroundTask()
    {
        
        self.beginBackgroundUpdateTask()
        checkStatusBool = true
        
        var userID = String()
        let childRef = FIRDatabase.database().reference()
        
        childRef.removeAllObservers()
        
        if  UserDefaults.standard.value(forKey: "UserId") != nil
        {
            print("saved id of the player itself = \(UserDefaults.standard.value(forKey: "UserId")!)")
            
            userID = UserDefaults.standard.value(forKey: "UserId") as! String
            let userName_saved = UserDefaults.standard.value(forKey: "userName") as! String
            
            childRef.child("Users").child(userID).updateChildValues(["Status": "Offline"])
        }
        
        
        print("UserDefaults.standard = \(UserDefaults.standard.value(forKey: "keyGenerated"))")
        
        var getUserCountGroups = Int()
        
        if UserDefaults.standard.value(forKey: "keyGenerated") != nil
        {
            
            childRef.child("groups").child(UserDefaults.standard.value(forKey: "playerSelectionString") as! String).observeSingleEvent(of: FIRDataEventType.value, with: { snapshot in
                
                print("groups snapshot value = \(snapshot.value)")
                
                if snapshot.value is NSNull
                {
                    print("snapshot is nsnull")
                }
                else
                {
                     if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                     {
                    if snapshot.hasChild(UserDefaults.standard.value(forKey: "keyGenerated") as! String)
                    {
                        print("group exists")
                        
                        let groupDict1 = snapshot.value as! NSDictionary
                        
                        let groupDict = groupDict1.value(forKey: (UserDefaults.standard.value(forKey: "keyGenerated") as! String)) as! NSDictionary
                        
                        print(groupDict)
                        getUserCountGroups = (groupDict.value(forKey: "users") as! NSDictionary).count
                        
                        childRef.child("groups").child(UserDefaults.standard.value(forKey: "playerSelectionString") as! String).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).child("users").observeSingleEvent(of: FIRDataEventType.value, with: { snapshot in
                            
                            print("getScoreWhenChanged 11 = \(snapshot.value)")
                            
                            if snapshot.value is NSNull
                            {
                                print("snapshot is nsnull")
                            }
                            else
                            {
                                var usersDict = snapshot.value as! NSDictionary
                                var scoreChangedStr = String()
                                
                                for value in usersDict
                                {
                                    let str = value.value as! String
                                    let keyStrUser = value.key as! String
                                    let savedUserID = UserDefaults.standard.value(forKey: "UserId") as! String
                                    
                                    if  savedUserID == keyStrUser as String
                                    {
                                        let tempStr = str.components(separatedBy: ":")[1]
                                        scoreChangedStr = tempStr.components(separatedBy: " ")[0]
                                        print("My score = \(scoreChangedStr)")
                                        
                                        print("ME = \(value.value)")
                                    
                                        
                                        let keyGenerated = UserDefaults.standard.value(forKey: "keyGenerated")
                                        
                                        if getUserCountGroups == 3 || getUserCountGroups == 4 || getUserCountGroups == 5 || getUserCountGroups == 6
                                        {
                                            
                                            if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                                            {
                                               childRef.child("groups").child(UserDefaults.standard.value(forKey: "playerSelectionString") as! String).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).child("users").child(savedUserID).setValue(nil)
                                            }
                                            
                                        }
                                        else if getUserCountGroups == 1
                                        {
                                           
                                            
                                            if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                                            {
                                                childRef.child("groups").child(UserDefaults.standard.value(forKey: "playerSelectionString") as! String).child(keyGenerated as! String).setValue(nil)
                                            }
                                            
                                            UserDefaults.standard.set(nil, forKey: "NewGroupKeyGenerated")
                                            UserDefaults.standard.set(nil, forKey: "keyGenerated")
                                            
                                            self.moveUserToHome = true

                                        }
                                        else
                                        {
                                            
                                    
                        if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                       {
                              childRef.child("groups").child(UserDefaults.standard.value(forKey: "playerSelectionString") as! String).child(stringArray).child("users").child(savedUserID).setValue(nil)
                       }
                        
                    

                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userLost"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gameQuitAds"), object: nil)

         
                                            
                                        }
                                        
                                    }
                                }
                            }
                            
                        })
                        
                    }
                    else
                    {
                        print("group id/key doesn't exist")
                    }
                    }
                }
                
                
                
            })
            
        }
        
            // End the background task.
            self.endBackgroundUpdateTask()
    }
    
    
    
    
    // MARK: - Begin Background tasks update Method
    func beginBackgroundUpdateTask() {
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
    }
    
    // MARK: - End Background Update tasks Method
    func endBackgroundUpdateTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }

    
    // MARK: - Application-Did-Enter-Background Method
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        self.doBackgroundTask()
    }
    
    
    // MARK: - Application-Will-Enter-Foreground Method
    func applicationWillEnterForeground(_ application: UIApplication)
    {
        
        UserDefaults.standard.set(("Online" as AnyObject), forKey: "Status")
        
        var userID = String()
        let childRef = FIRDatabase.database().reference()
        if  UserDefaults.standard.value(forKey: "UserId") != nil
        {
            print("saved id of the player itself = \(UserDefaults.standard.value(forKey: "UserId")!)")
            
            userID = UserDefaults.standard.value(forKey: "UserId") as! String
            let userName_saved = UserDefaults.standard.value(forKey: "userName") as! String
            
            childRef.child("Users").child(userID).updateChildValues(["Status": "Online"])
        }
        
    }
    
    
    
    
    // MARK: - Application-Did-Become-Active Method
    func applicationDidBecomeActive(_ application: UIApplication)
    {
        UserDefaults.standard.set(("Online" as AnyObject), forKey: "Status")
    
        var userID = String()
        let childRef = FIRDatabase.database().reference()
        if  UserDefaults.standard.value(forKey: "UserId") != nil
        {
            print("saved id of the player itself = \(UserDefaults.standard.value(forKey: "UserId")!)")
            
            userID = UserDefaults.standard.value(forKey: "UserId") as! String
            let userName_saved = UserDefaults.standard.value(forKey: "userName") as! String
            
           childRef.child("Users").child(userID).updateChildValues(["Status": "Online"])
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
    }
    
    
    
    // MARK: - Application-Will-Terminate Method
    func applicationWillTerminate(_ application: UIApplication)
    {
        self.doBackgroundTask()
    }

}

