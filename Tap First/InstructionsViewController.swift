//
//  InstructionsViewController.swift
//  Tap First
//
//  Created by brst on 06/01/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController
{
    
    
    // MARK: - Outlets Connection
    @IBOutlet var doneBtn: UIButton!
    @IBOutlet var tutorialsBackgroundImage: UIImageView!
    @IBOutlet var wrapperView: UIView!
    @IBOutlet var description_txtView: UITextView!
    

    // MARK: - View-Did-Load Method
    override func viewDidLoad()
    {
        super.viewDidLoad()

        UIApplication.shared.isStatusBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        
        description_txtView.text = "RANDOM CIRCLES IN VARIOUS\nCOLOURS WILL APPEAR\nACROSS IN DIFFERENT TIME\nINTERVALS.\n\nTAP THE CIRCLE FIRST BEFORE\nYOUR OPPONENT.\n\nEACH CIRCLE YOU TAP GIVES\nYOU 1 POINT,REACH 10\nPOINTS TO WIN!"

        description_txtView.textAlignment = NSTextAlignment.left
        description_txtView.textColor = UIColor.white
        description_txtView.font = UIFont(name: "ChelaOne-Regular", size: 17.0)
    }
    
    
    
    // MARK: - Done Btn Action
    @IBAction func DoneBtn_Action(_ sender: Any)
    {
        let playGameScreen = self.storyboard?.instantiateViewController(withIdentifier: "PlayGameViewController") as! PlayGameViewController
        UserDefaults.standard.setValue("isTutorialScreen_True", forKey: "isTutorialScreen")
        UserDefaults.standard.set(0, forKey: "displayAds_value_PlayBtn")
        UserDefaults.standard.set(0, forKey: "displayAds_value_gameCompleted")
//        UserDefaults.standard.set(0, forKey: "displayAds_value_gameQuit")
        self.navigationController?.pushViewController(playGameScreen, animated: true)
    }
    

    // MARK: - Did-Receive-Memory-Warning
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
