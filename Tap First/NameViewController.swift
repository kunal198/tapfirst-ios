//
//  NameViewController.swift
//  Tap First
//
//  Created by brst on 27/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation

class NameViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate
{
    
    // MARK: - Variable Declarations
    var audioPlayer:AVAudioPlayer!
    let limitLength = 6
    var firebaseUsersArray = NSMutableArray()
    var getNetworkStatus = String()
    
    // MARK: - Outlets Connection
    @IBOutlet var name_txtField: UITextField!
    @IBOutlet var goBtn: UIButton!
    
    // MARK: - View-Did-Load Method
    override func viewDidLoad()
    {
        super.viewDidLoad()


        name_txtField.attributedPlaceholder = NSAttributedString(string: "PLEASE ENTER YOUR NAME",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(NameViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reach().monitorReachabilityChanges()
    }

    
    // MARK: - Get Network Status Reachibility Method
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        print("userInfo for network reachibility = \(userInfo)")
        
        getNetworkStatus = (userInfo?["Status"] as? String)!
        print("userInfo for network reachibility in string = \(getNetworkStatus)")
        
        if getNetworkStatus == "Offline"
        {

            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - View-Will-Appear Method
    override func viewWillAppear(_ animated: Bool)
    {

    }
    
    
    // MARK: - Play Audio File In Background Method
    func PlaySoundInBackground()
    {
        let audioFilePath = Bundle.main.path(forResource: "Playscreen, enter username screen, choose how many players screen and Match report screen. ", ofType: "mp3")
        let url = URL(fileURLWithPath: audioFilePath!)
        
        do
        {
            let sound = try AVAudioPlayer(contentsOf: url)
            audioPlayer = sound
            sound.play()
        }
        catch
        {
            // couldn't load file :(
            print("couldn't load file")
        }

    }
    
    
    // MARK: - Go Btn Action
    @IBAction func goBtn_Action(_ sender: Any)
    {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .online(.wwan):
            print("Connected via WWAN")
            registerUserToFirebase()
        case .online(.wiFi):
            print("Connected via WiFi")
            registerUserToFirebase()
        }
    }
    
    
    
    
    func registerUserToFirebase()
    {
        if name_txtField.text == ""
        {
            let alert = UIAlertController(title: "", message: "Please enter your name", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            // 1
            let rootRef = FIRDatabase.database().reference()
            
            
            // fetch firebase instance token
            let refreshedToken = FIRInstanceID.instanceID().token()
            let childRef = FIRDatabase.database().reference()
            UserDefaults.standard.set((refreshedToken as AnyObject) ,forKey: "Token")
            
            // save user into the firebase realtime database
            let keyGenerated = childRef.child("posts").childByAutoId().key
            childRef.child("Users").child(keyGenerated).child("Name").setValue(name_txtField.text)
            childRef.child("Users").child(keyGenerated).child("Token").setValue(refreshedToken)
            childRef.child("Users").child(keyGenerated).child("UserId").setValue(keyGenerated)
            childRef.child("Users").child(keyGenerated).child("Status").setValue("Online")
            
            UserDefaults.standard.set((name_txtField.text as AnyObject), forKey: "userName")
            UserDefaults.standard.set((keyGenerated as AnyObject), forKey: "UserId")
            UserDefaults.standard.set(("Online" as AnyObject), forKey: "Status")
            UserDefaults.standard.set((refreshedToken as AnyObject), forKey: "Token")
            
            let nameScreen = self.storyboard?.instantiateViewController(withIdentifier: "PlayerSelectionViewController") as! PlayerSelectionViewController
            
            if audioPlayer != nil
            {
                if audioPlayer.isPlaying
                {
                    audioPlayer.stop()
                }
                
            }
            
            self.navigationController?.pushViewController(nameScreen, animated: true)
            
        }
        
    }
    
    
    
    // MARK: - Back Btn Method
    @IBAction func backBtn_Action(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - UI-TextField Delegates
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        name_txtField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= limitLength
    }
    
    
    // MARK: - Did-Receive-Memory-Warning Method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
