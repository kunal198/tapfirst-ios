//
//  PlayGameViewController.swift
//  Tap First
//
//  Created by brst on 27/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import GoogleMobileAds
import AVFoundation


class PlayGameViewController: UIViewController,GADInterstitialDelegate
{

    // MARK: - Outlets Connection
    @IBOutlet var playBtn: UIButton!
    
    
    // MARK: - Variables Declaration
    var dismissAds = Bool()
    var interstitialAd : GADInterstitial!

    
    // MARK: - View-Did-Load Method
    override func viewDidLoad()
    {
        super.viewDidLoad()

        dismissAds = false
        
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(PlayGameViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        Reach().monitorReachabilityChanges()
      }
    
    // MARK: - Get Network Status Reachibility Method
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        print("userInfo for network reachibility = \(userInfo)")
    }
    
    // MARK: - View-Will-Appear Method
    override func viewWillAppear(_ animated: Bool)
    {
        if audioPlayer != nil
        {
            if audioPlayer.isPlaying == false
            {
                PlaySoundInBackground()
            }
        }
        else
        {
            PlaySoundInBackground()
        }
    }
    
    
    // MARK: - Play Audio File In Background Method
    func PlaySoundInBackground()
    {
        let url = Bundle.main.url(forResource: "Playscreen, enter username screen, choose how many players screen and Match report screen. ", withExtension: ".mp3")!
        
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.numberOfLoops = -1
            guard let player = audioPlayer else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    
    
    
    
    // MARK: - Play Btn Method
    @IBAction func playBtn_Action(_ sender: Any)
    {
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected")
        case .online(.wwan):
            print("Connected via WWAN")
        case .online(.wiFi):
            print("Connected via WiFi")
        }
        
        if (UserDefaults.standard.value(forKey: "displayAds_value_PlayBtn") != nil)
        {
            var displayAds_value_PlayBtn = Int()
            displayAds_value_PlayBtn = UserDefaults.standard.value(forKey: "displayAds_value_PlayBtn") as! Int
            print("displayAds_value before increment = \(displayAds_value_PlayBtn)")
            
            displayAds_value_PlayBtn = displayAds_value_PlayBtn + 1
            UserDefaults.standard.set(displayAds_value_PlayBtn, forKey: "displayAds_value_PlayBtn")
            print("displayAds_value after increment = \(displayAds_value_PlayBtn)")
            
            if displayAds_value_PlayBtn % 3 == 0
            {
                // Display the intertitial ad
                interstitialAd = createAndLoadInterstitial()
            }
            else
            {
                if UserDefaults.standard.value(forKey: "Token") != nil
                {
                    UserDefaults.standard.set(("Online" as AnyObject), forKey: "Status")
                    let nameScreen = self.storyboard?.instantiateViewController(withIdentifier: "PlayerSelectionViewController") as! PlayerSelectionViewController
                    self.navigationController?.pushViewController(nameScreen, animated: true)
                }
                else
                {
                    let nameScreen = self.storyboard?.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
                    self.navigationController?.pushViewController(nameScreen, animated: true)
                }
                
            }
            
        }
        else
        {
            if UserDefaults.standard.value(forKey: "Token") != nil
            {
                let nameScreen = self.storyboard?.instantiateViewController(withIdentifier: "PlayerSelectionViewController") as! PlayerSelectionViewController
                self.navigationController?.pushViewController(nameScreen, animated: true)
            }
            else
            {
                let nameScreen = self.storyboard?.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
                self.navigationController?.pushViewController(nameScreen, animated: true)
            }
            
        }
        
    }
    

    // MARK: - Help methods
     func createAndLoadInterstitial() -> GADInterstitial? {
//        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-8501671653071605/2568258533") //Test ID
        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-3259941691864510/7380319980") //Client ID
        
        guard let interstitial = interstitialAd else {
            return nil
        }
        
        let request = GADRequest()
//        request.testDevices = [ kGADSimulatorID,"5c8891a4a24b9906205ad319f72f9ffe" ]
        interstitial.load(request)
        interstitial.delegate = self
        
        return interstitial
    }

    
    
    
    
    //MARK:- INSTERSTITIAL ADS DELEGATES
    
    /// Called when an interstitial ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial!)
    {
        print("interstitialDidReceiveAd")
        interstitialAd!.present(fromRootViewController: self)
    }
    
    
    /// Called when an interstitial ad request failed.
    func interstitial(_ ad: GADInterstitial!, didFailToReceiveAdWithError error: GADRequestError!)
    {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Called just before presenting an interstitial.
    func interstitialWillPresentScreen(_ ad: GADInterstitial!)
    {
        print("interstitialWillPresentScreen")
    }
    
    /// Called before the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial!)
    {
        print("interstitialWillDismissScreen")
        
        dismissAds = true
        
    }
    
    
    /// Called just before the application will background or terminate because the user clicked on an
    /// ad that will launch another app (such as the App Store).
    func interstitialWillLeaveApplication(_ ad: GADInterstitial!)
    {
        print("interstitialWillLeaveApplication")
    }
    
    

    
    // MARK: - Did-Receive-Memory-Warning Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
