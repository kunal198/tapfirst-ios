//
//  StartPlayingGameViewController.swift
//  Tap First
//
//  Created by brst on 27/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import AVFoundation

class StartPlayingGameViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate,GADBannerViewDelegate,GADInterstitialDelegate
{

    // MARK: - Variables Declaration
     var gameOver  = Bool()
     var ballTapped  = Bool()
     var tapGesture  = UITapGestureRecognizer()
     var interstitialAd : GADInterstitial!
     var audioPlayingInBackground:AVAudioPlayer!
     var audioOnTap:AVAudioPlayer!
     var audioPlayer_gameOver:AVAudioPlayer!
     var getUserKey = String()
    var playerSelectionString = String()
    var onTapValueSet = String()
    var imagesCounter = Int()
    var childRef = FIRDatabase.database().reference()
    var getPlayerDict = NSDictionary()
    var randomImagesArray = [String]()
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var playerNameArray = NSArray()
    var playerScoreArray = NSArray()
    var resultArr = NSMutableArray()
    var pairUserArray = NSMutableArray()
    var score = String()
    var alert = UIAlertController()
    var checkPush = String()
    var scoreSaved = Int()
    var userStatus = String()
    var MyStatus = String()
    var getMyScoreCount = String()
    var timeIntervalArray = [Double]()
     var firstMyNameArray = [String]()
    var MeArray = NSMutableArray()
    var otherPlayersArray = NSMutableArray()
    var getMyKey = String()
    var getNetworkStatus = String()
    

    // MARK: - Outlets Connection
    @IBOutlet var lose_cancelBtn: UIButton!
    @IBOutlet var lose_retryBtn: UIButton!
    @IBOutlet var lose_view: UIView!
    @IBOutlet var WinView: UIView!
    @IBOutlet var win_cancelBtn: UIButton!
    @IBOutlet var win_okBtn: UIButton!
    @IBOutlet var bannerView_mobileAds: GADBannerView!
    @IBOutlet var tapBallBtn: UIButton!
    @IBOutlet var wrapperView: UIView!
    @IBOutlet var playerName_collectionView: UICollectionView!
    
    
    
     // MARK: - View-Did-Load Method
    override func viewDidLoad()
    {
        gameOver = false

        super.viewDidLoad()
        
        gameCompletionAds_bool = false
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userLost),
                                               name: NSNotification.Name(rawValue: "userLost"),
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showInterstitialAdsOnGameQuit),
                                               name: NSNotification.Name(rawValue: "gameQuitAds"),
                                               object: nil)
        
        imagesCounter = 0
        randomImagesArray = ["Tap Image Sky Blue","Tap Image Red","Tap Image Purple","Tap Image Light Green","Tap Image Dark Blue","Tap Image Dark Green","Tap Image Brown","Tap Image Yellow"]
        timeIntervalArray = [2.0, 5.0, 1.0, 6.0, 7.0, 3.0, 8.0, 4.0]
        
//        initializeUserDefaultsValues()
        backgroundBoolForCountDown = false

        if UserDefaults.standard.value(forKey: "CurrentUsersArray") != nil
        {
            self.pairUserArray = (UserDefaults.standard.value(forKey: "CurrentUsersArray") as! NSArray).mutableCopy() as! NSMutableArray
            //Print("self.pairUserArray = \(self.pairUserArray)")
            getPlayerDict = self.pairUserArray[0] as! NSDictionary
            //Print("getPlayerDict = \(getPlayerDict)")
        }
        
        
        for value in getPlayerDict
        {
//            resultArr.add(value.value)
//            //Print("resultArr = \(resultArr)")
            let userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
            if userID_saved == value.key as! String
            {
                let str = value.value as! String
                
                getMyKey = value.key as! String
                
                let tempStr = str.components(separatedBy: ":")[1]
                getMyScoreCount = tempStr.components(separatedBy: " ")[0]
                //Print("score = \(getMyScoreCount)")
                //Print("resultArr = \(resultArr.count)")
                
                MyStatus = tempStr.components(separatedBy: "status ")[1]
                //Print("MyStatus = \(MyStatus)")
                
                MeArray.add(value.value)
            }
            else
            {
                 otherPlayersArray.add(value.value)
            }

            
        }

        
        for item in self.otherPlayersArray
        {
//            if !(self.MeArray.contains(item))
//            {
                self.MeArray.add(item)
//            }
        }

        
        
        let request = GADRequest()
//        request.testDevices = ["bd11d4008102a07d018f73cb67dcc2e1","2077ef9a63d2b398840261c8221a0c9b","9ada2ece428c0b1e0e44701ad8d8a8c296857ea1",kGADSimulatorID]
        
        
//        bannerView_mobileAds.adUnitID = "ca-app-pub-8501671653071605/1974659335" //Test ID
        bannerView_mobileAds.adUnitID = "ca-app-pub-3259941691864510/4287252782" //Client ID
        bannerView_mobileAds.delegate = self
        bannerView_mobileAds.rootViewController = self
        // Request a Google Ad
        bannerView_mobileAds.load(GADRequest())
        
       
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(StartPlayingGameViewController.Tap))  //Tap function will call when user tap on button
        tapGesture.numberOfTapsRequired = 1
        tapBallBtn.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(StartPlayingGameViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
//        Reach().monitorReachabilityChanges()
        
    
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected on countdown screen")
            
             gameQuitAds_bool = true
            
                            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                print("You've pressed OK button");
                                
                                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "userLost"), object: nil)
                                
                                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gameQuitAds"), object: nil)
            
                                for controller in self.navigationController!.viewControllers as Array
                                {
                                    if controller.isKind(of: PlayGameViewController.self)
                                    {
                                        self.navigationController?.popToViewController(controller , animated: true)
                                        break
                                    }
                                }
            
            
            
                            }
            
                            alert.addAction(OKAction)
            
                            if presentedViewController == nil
                            {
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                self.dismiss(animated: false) { () -> Void in
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
            
            
        case .online(.wwan), .online(.wiFi):
            print("Connected via WWAN on playing game screen")
    }
        
}
    
    
    // MARK: - Get Network Status Reachibility Method
    func networkStatusChanged(_ notification: Notification)
    {
        let userInfo = (notification as NSNotification).userInfo
        print("userInfo for network reachibility = \(userInfo)")
        
        getNetworkStatus = (userInfo?["Status"] as? String)!
        print("userInfo for network reachibility in string = \(getNetworkStatus)")
        
        if getNetworkStatus == "Offline"
        {
             gameQuitAds_bool = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                print("You've pressed OK button");
                
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "userLost"), object: nil)
                
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gameQuitAds"), object: nil)
                
                for controller in self.navigationController!.viewControllers as Array
                {
                    if controller.isKind(of: PlayGameViewController.self)
                    {
                        self.navigationController?.popToViewController(controller , animated: true)
                        break
                    }
                }
                
            }
            
            alert.addAction(OKAction)
            
            if presentedViewController == nil
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.dismiss(animated: false) { () -> Void in
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    
    
    
    
    // MARK: - View-Will-Appear Method
    override func viewWillAppear(_ animated: Bool)
    {
        if gameQuitAds_bool == true
        {
            return
        }
        
        PlaySoundInBackground()

        if gameCompletionAds_bool == false
        {
            childRef.child("groups").observeSingleEvent(of: FIRDataEventType.value, with: { snapshot in
            
                print("getScoreWhenChanged 11 = \(snapshot)")

                if !(snapshot.value is NSNull)
                {
                    self.childRef.child("groups").child(UserDefaults.standard.value(forKey: "playerSelectionString") as! String).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).child("users").observeSingleEvent(of: FIRDataEventType.value, with: { snapshot in
                        
                        print("getScoreWhenChanged 11 = \(snapshot.value)")
                        
                        if snapshot.value is NSNull
                        {
                            print("snapshot is nsnull")
                        }
                        else
                        {
                            let usersDict = snapshot.value as! NSDictionary
                            
                            if usersDict.count == 1
                            {
                                
                                for value in usersDict
                                {
                                    
                                    let str = value.value as! String
                                    let keyStrUser = value.key as! String
                                    let savedUserID = UserDefaults.standard.value(forKey: "UserId") as! String
                                    
                                    let tempStr = str.components(separatedBy: ":")[1]
                                    //                        self.score = tempStr.components(separatedBy: " ")[0]
                                    //Print("score = \(self.score)")
                                    
                                    let MyOnlineStatus = tempStr.components(separatedBy: "status ")[1]
                                    //Print("userStatus = \(self.userStatus)")
                                    
                                    if  savedUserID == keyStrUser as String && MyOnlineStatus == "Online"
                                    {
                                        if self.audioPlayingInBackground.isPlaying
                                        {
                                            self.audioPlayingInBackground.stop()
                                        }
                                        
                                        print("WOOOONNNNNNNN ===========")
                                        self.PlaySoundOnWin()
                                        
                                        if (UserDefaults.standard.value(forKey: "totalWin_score") != nil)
                                        {
                                            var totalWin_score = Int()
                                            totalWin_score = UserDefaults.standard.value(forKey: "totalWin_score") as! Int
                                            //Print("total win score before increment = \(totalWin_score)")
                                            
                                            totalWin_score = totalWin_score + 1
                                            //Print("total win score after increment = \(totalWin_score)")
                                            
                                            UserDefaults.standard.set(totalWin_score, forKey: "totalWin_score")
                                        }
                                        
                                        
                                        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                                            
                                            self.WinView.isHidden = false
                                            self.WinView.alpha = 1.0
                                            
                                        }, completion: nil)
                                        
                                        self.tapBallBtn.isUserInteractionEnabled = false
                                        
                                        
                                        // save user into the firebase realtime database
                                        let keyGenerated = UserDefaults.standard.value(forKey: "keyGenerated")
                                        //Print("keyGenerated = \(keyGenerated)")
                                        
                                        if keyGenerated != nil
                                        {
                                            self.childRef.child("groups").child(self.playerSelectionString).child(keyGenerated as! String).setValue(nil)
                                            
                                            UserDefaults.standard.set(nil, forKey: "NewGroupKeyGenerated")
                                            UserDefaults.standard.set(nil, forKey: "keyGenerated")
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            else
                            {
                                self.getScoreWhenChanged()
                            }
                            
                        }
                        
                        
                    })
                }
                else
                {
                    print("group doesnot exists")
                    
                    self.userLost()

                    if gameQuitAds_bool == false
                    {
                        self.showInterstitialAdsOnGameQuit()
                    }
                    else
                    {
                        gameQuitAds_bool = false

                        print("gameQuitAds_bool == true")
                    }
                }
                
            })
            
        }
        else
        {
            print("not the case to display game completion ads")
            gameCompletionAds_bool = false
        }
      
    }

    
    // MARK: - View-Will-DisAppear Method
    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "userLost"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gameQuitAds"), object: nil)
    }
    
    
     // MARK: - Get Score for Two/Three/Four/Five/Six Users
    func getScoreWhenChanged()
    {
        let childRef = FIRDatabase.database().reference()
        
         childRef.child("groups").observeSingleEvent(of: FIRDataEventType.value, with: { snapshot in
            
            
            if snapshot.value is NSNull || UserDefaults.standard.value(forKey: "keyGenerated") as? String == nil
            {
               //print("snapshot for groups is nsnull")
            }
            else
            {
                print(UserDefaults.standard.value(forKey: "keyGenerated") as! String)
                
                childRef.child("groups").child(self.playerSelectionString).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).observe(FIRDataEventType.childChanged, with: { snapshot in
                    
                   //print("getScoreWhenChanged method = \(snapshot.value)")
                    
                    if snapshot.value is NSNull
                    {
                       //print("snapshot group id is nsnull")
                    }
                    else
                    {
                        if snapshot.value is NSString
                        {
                            return;
                        }
                        
                        self.getPlayerDict = snapshot.value as! NSDictionary
                       print("getPlayerDict in get score method = \(self.getPlayerDict)")
                        
                        if self.resultArr.count > 0
                        {
                            self.resultArr.removeAllObjects()
                        }
                        
                        if self.firstMyNameArray.count > 0
                        {
                            self.firstMyNameArray.removeAll()
                        }
                        
                        if self.MeArray.count > 0
                        {
                            self.MeArray.removeAllObjects()
                        }
                        
                        if self.otherPlayersArray.count > 0
                        {
                            self.otherPlayersArray.removeAllObjects()
                        }
                        
                        
                        
                        if self.getPlayerDict.count > 0
                        {
                            self.tapBallBtn.isHidden  = true
                            self.tapBallBtn.isUserInteractionEnabled = false
                            self.tapGesture.isEnabled = false
                           //print(self.imagesCounter)
                           //print(self.randomImagesArray.count)
                            
                            if self.imagesCounter >= self.randomImagesArray.count
                            {
                                self.imagesCounter = 0
                            }
                            else
                            {
                                if self.imagesCounter == 7
                                {
                                    self.imagesCounter = 0
                                }
                                else
                                {
                                    self.imagesCounter = self.imagesCounter + 1
                                }
                            }
                            
                            
                            if self.imagesCounter < self.randomImagesArray.count
                            {
                                // Find the button's width and height
                                let buttonWidth = self.tapBallBtn.frame.width
                                let buttonHeight = self.tapBallBtn.frame.height
                                
                                // Find the width and height of the enclosing view
                                let viewWidth = self.tapBallBtn.superview!.bounds.width
                                let viewHeight = self.tapBallBtn.superview!.bounds.height
                                
                                // Compute width and height of the area to contain the button's center
                                let xwidth = viewWidth - buttonWidth
                                let yheight = viewHeight - buttonHeight
                                
                                // Generate a random x and y offset
                                let xoffset = CGFloat(arc4random_uniform(UInt32(xwidth)))
                                let yoffset = CGFloat(arc4random_uniform(UInt32(yheight)))
                                
                                // Offset the button's center by the random offsets.
                                self.tapBallBtn.center.x = xoffset + buttonWidth / 2
                                self.tapBallBtn.center.y = yoffset + buttonHeight / 2
                                
                                
                                //Print("randomImagesArray[i] = \(self.randomImagesArray[self.imagesCounter])")
                                //Print("self.timeIntervalArray[self.imagesCounter] = \(self.timeIntervalArray[self.imagesCounter])")
                                
                                let delayInSeconds = self.timeIntervalArray[self.imagesCounter]
                                
                               //print(self.imagesCounter)
                                
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds){
                                
                                    self.ballTapped = false
                                    self.tapBallBtn.isHidden = false
                                    self.tapBallBtn.isUserInteractionEnabled = true
                                    self.tapGesture.isEnabled = true
                                }
                                
                                let image = UIImage(named: self.randomImagesArray[self.imagesCounter]) as UIImage?
                                self.tapBallBtn.setBackgroundImage(image, for: UIControlState.normal)
                            }
                            
                        }
                        
                        
                        for value in self.getPlayerDict
                        {
                            //                self.resultArr.add(value.value)
                            //                //Print("resultArr = \(self.resultArr)")
                            
                            let str = value.value as! String
                            
                            let tempStr = str.components(separatedBy: ":")[1]
                            self.score = tempStr.components(separatedBy: " ")[0]
                            //Print("score = \(self.score)")
                            
                            self.userStatus = tempStr.components(separatedBy: "status ")[1]
                            //Print("userStatus = \(self.userStatus)")
                            
                            
                            let keyStrUser = value.key as! String
                            let savedUserID = UserDefaults.standard.value(forKey: "UserId") as! String
                            
                            
                            if  savedUserID == keyStrUser as String
                            {
                                self.getMyScoreCount = tempStr.components(separatedBy: " ")[0]
                                //Print("getMyScoreCount = \(self.getMyScoreCount)")
                                
                                UserDefaults.standard.set(self.getMyScoreCount, forKey: "getMyScoreCount")
                                
                                self.MyStatus = tempStr.components(separatedBy: "status ")[1]
                                //Print("MyStatus = \(self.MyStatus)")
                                
                                self.MeArray.add(value.value)
                            }
                            else
                            {
                                self.otherPlayersArray.add(value.value)
                            }
                            
                            
                            //Print("MeArray = \(self.MeArray)")
                            //Print("otherPlayersArray = \(self.otherPlayersArray)")
                            
                            if self.getPlayerDict.count == 1 && self.userStatus == "Online" &&  savedUserID == keyStrUser as String
                            {
                                if self.audioPlayingInBackground.isPlaying
                                {
                                    self.audioPlayingInBackground.stop()
                                }
                                
                                print("WOOOONNNNNNNN  2")
                                self.PlaySoundOnWin()
                                
                                if (UserDefaults.standard.value(forKey: "totalWin_score") != nil)
                                {
                                    var totalWin_score = Int()
                                    totalWin_score = UserDefaults.standard.value(forKey: "totalWin_score") as! Int
                                    //Print("total win score before increment = \(totalWin_score)")
                                    
                                    totalWin_score = totalWin_score + 1
                                    //Print("total win score after increment = \(totalWin_score)")
                                    
                                    UserDefaults.standard.set(totalWin_score, forKey: "totalWin_score")
                                }
                                
                                
                                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                                    
                                    self.WinView.isHidden = false
                                    self.WinView.alpha = 1.0
                                    
                                }, completion: nil)
                                
                                self.tapBallBtn.isUserInteractionEnabled = false
                                
                                
                                // save user into the firebase realtime database
                                let keyGenerated = UserDefaults.standard.value(forKey: "keyGenerated")
                                //Print("keyGenerated = \(keyGenerated)")
                                
                                if keyGenerated != nil
                                {
                                    
                                    if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                                    {
                                        self.childRef.child("groups").child(self.playerSelectionString).child(keyGenerated as! String).setValue(nil)
                                    }
                                    
                                    UserDefaults.standard.set(nil, forKey: "NewGroupKeyGenerated")
                                    UserDefaults.standard.set(nil, forKey: "keyGenerated")
                                }
                                
                            }
                            
                            
                            
                            if self.userStatus == "Offline" && self.getPlayerDict.count >= 2
                            {
                                
                                if self.audioPlayingInBackground.isPlaying
                                {
                                    self.audioPlayingInBackground.stop()
                                }
                                
                                if  savedUserID == keyStrUser as String
                                {
                                    self.userLost()
                                }
                                else
                                {
                                    print("WOOOONNNNNNNN  3")
                                    self.PlaySoundOnWin()
                                    
                                    if (UserDefaults.standard.value(forKey: "totalWin_score") != nil)
                                    {
                                        var totalWin_score = Int()
                                        totalWin_score = UserDefaults.standard.value(forKey: "totalWin_score") as! Int
                                        //Print("total win score before increment = \(totalWin_score)")
                                        
                                        totalWin_score = totalWin_score + 1
                                        //Print("total win score after increment = \(totalWin_score)")
                                        
                                        UserDefaults.standard.set(totalWin_score, forKey: "totalWin_score")
                                    }
                                    
                                    
                                    UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                                        
                                        self.WinView.isHidden = false
                                        self.WinView.alpha = 1.0
                                        
                                    }, completion: nil)
                                    
                                    self.tapBallBtn.isUserInteractionEnabled = false
                                }
                                
                            }
                            
                            
                            if self.score == "10"
                            {
                                
                                if self.audioPlayingInBackground.isPlaying
                                {
                                    self.audioPlayingInBackground.stop()
                                }
                                
                                
                                if  savedUserID == keyStrUser as String
                                {
                                    print("WOOOONNNNNNNN  1")
                                    self.PlaySoundOnWin()
                                    gameCompletionAds_bool = true
                                    self.showInterstitialAdsOnCompletion()
                                    
                                    if (UserDefaults.standard.value(forKey: "totalWin_score") != nil)
                                    {
                                        var totalWin_score = Int()
                                        totalWin_score = UserDefaults.standard.value(forKey: "totalWin_score") as! Int
                                        //Print("total win score before increment = \(totalWin_score)")
                                        
                                        totalWin_score = totalWin_score + 1
                                        //Print("total win score after increment = \(totalWin_score)")
                                        
                                        UserDefaults.standard.set(totalWin_score, forKey: "totalWin_score")
                                        
                                    }
                                    
                                    UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                                        
                                        self.WinView.isHidden = false
                                        self.WinView.alpha = 1.0
                                        
                                    }, completion: nil)
                                    
                                    self.tapBallBtn.isUserInteractionEnabled = false
                                }
                                    
                                else
                                {
                                      self.userLost()
                                }
                                
                                // save user into the firebase realtime database
                                let keyGenerated = UserDefaults.standard.value(forKey: "keyGenerated")
                                //Print("keyGenerated = \(keyGenerated)")
                                
                                if keyGenerated != nil
                                {
                                    
                                    if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                                    {
                                        self.childRef.child("groups").child(self.playerSelectionString).child(keyGenerated as! String).setValue(nil)
                                    }
                                    UserDefaults.standard.set(nil, forKey: "NewGroupKeyGenerated")
                                    UserDefaults.standard.set(nil, forKey: "keyGenerated")
                                }
                                
                            }
                            self.playerName_collectionView.reloadData()
                        }
                        
                        for item in self.otherPlayersArray
                        {
//                            if !(self.MeArray.contains(item))
//                            {
                                self.MeArray.add(item)
//                            }
                        
                        }
                        
                        self.playerName_collectionView.reloadData()
                    }
            
                    
                })
            }
            
        })
        
        
    }
    
    
    
    
    
    
    
    // MARK: - Showing Alert when player Lost the Game
    func userLost()
    {
        if audioPlayingInBackground != nil
        {
            if self.audioPlayingInBackground.isPlaying
            {
                self.audioPlayingInBackground.stop()
            }
        }
        
        if (UserDefaults.standard.value(forKey: "totalLose_score") != nil)
        {
            var totalLose_score = Int()
            totalLose_score = UserDefaults.standard.value(forKey: "totalLose_score") as! Int
            //Print("total win score before increment = \(totalWin_score)")
            totalLose_score = totalLose_score + 1
            //Print("total win score after increment = \(totalWin_score)")
            UserDefaults.standard.set(totalLose_score, forKey: "totalLose_score")
        }

        
        self.PlaySoundOnLose()
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            
            self.lose_view.isHidden = false
            self.lose_view.alpha = 1.0
            
        }, completion: nil)
        
        self.tapBallBtn.isUserInteractionEnabled = false

    }

    
     // MARK: - Play Audio File In Background Method
    func PlaySoundInBackground()
    {
        
       if audioPlayer != nil
       {
        if audioPlayer.isPlaying
        {
            audioPlayer.stop()
        }
        }
        
        let url = Bundle.main.url(forResource: "Game play screen", withExtension: ".mp3")!
        
        do
        {
            audioPlayingInBackground = try AVAudioPlayer(contentsOf: url)
            audioPlayingInBackground.numberOfLoops = -1
            guard let player = audioPlayingInBackground else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            //Print(error.localizedDescription)
        }
        
    }

    
   // MARK: - Play Audio File On Tap Method
    func PlaySoundOnTap()
    {
        let url = Bundle.main.url(forResource: "Circle click", withExtension: ".mp3")!
        do
        {
            audioOnTap = try AVAudioPlayer(contentsOf: url)
            guard let player = audioOnTap else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            //Print(error.localizedDescription)
        }
        
    }

    
    
    // MARK: - Play Audio File On Player Win the Match Method
    func PlaySoundOnWin()
    {
        gameOver = true
        let url = Bundle.main.url(forResource: "Win", withExtension: ".mp3")!
        do
        {
            audioPlayer_gameOver = try AVAudioPlayer(contentsOf: url)
            guard let player = audioPlayer_gameOver else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            //Print(error.localizedDescription)
        }
        
    }

    
    // MARK: - Play Audio File On Lose the Match Method
    func PlaySoundOnLose()
    {
        gameOver = true

        let url = Bundle.main.url(forResource: "Lose", withExtension: ".mp3")!
        do
        {
            audioPlayer_gameOver = try AVAudioPlayer(contentsOf: url)
            guard let player = audioPlayer_gameOver else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            //Print(error.localizedDescription)
        }
        
    }

    
    
    
    // MARK: - GADBannerViewDelegate methods
    func adViewDidReceiveAd(_ bannerView: GADBannerView!)
    {
        //Print("Banner loaded successfully")
        
    }
    
    func adView(_ bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!)
    {
        //Print("Fail to receive ads")
        //Print(error)
        
    }

    
    // MARK: - Gesture Method On Btn's Tap
    func Tap()
    {
        if self.ballTapped == false
        {
            let childRef = FIRDatabase.database().reference()
            self.ballTapped = true

            childRef.child("groups").child(playerSelectionString).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).child("points").setValue(UserDefaults.standard.value(forKey: "UserId") as! String){ (error, ref) -> Void in
                
                if (error != nil)
                {
                    
                }
                else
                {
                    self.increaseScore()
                }
            }

        }
             //Print("Tap happend")
     }
    
    
    
    // MARK: - Increase the Score of the Player Method
    func increaseScore()
    {
        PlaySoundOnTap()
        
        let userName_saved = UserDefaults.standard.value(forKey: "userName") as! String
        let userID_saved = UserDefaults.standard.value(forKey: "UserId") as! String
        let userStatus_saved = UserDefaults.standard.value(forKey: "Status") as! String
        
        scoreSaved = Int(getMyScoreCount)!
        scoreSaved = scoreSaved + 1
        
        let aStr = String(format: "%@:%d status %@", userName_saved,scoreSaved,userStatus_saved)
        
        
        // save user into the firebase realtime database
        let keyGenerated = UserDefaults.standard.value(forKey: "keyGenerated")
        //Print("keyGenerated = \(keyGenerated)")
        let usersPairSaved = UserDefaults.standard.value(forKey: "pairUserCount")
        //Print("usersPairSaved = \(usersPairSaved)")
    

            self.childRef.child("groups").child(playerSelectionString).child(keyGenerated as! String).child("users").child(userID_saved).setValue(aStr){ (error, ref) -> Void in
                
                if (error != nil)
                {
                    
                }
                else
                {
                   if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                    {
                        self.childRef.child("groups").child(self.playerSelectionString).child(stringArray).child("points").setValue("Empty")
                    }
                }
        }
    }
    
    
    
    // MARK: - Game Over Action
    @IBAction func GameOver_Action(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            //Print("win ok btn")
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.WinView.isHidden = true
                self.WinView.alpha = 0.0
                
            }, completion: nil)
        }
        else if sender.tag == 2
        {
            //Print("win cancel btn")
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.WinView.isHidden = true
                self.WinView.alpha = 0.0
                
            }, completion: nil)
        }
        else if sender.tag == 3
        {
            //Print("lose retry btn")
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.lose_view.isHidden = true
                self.lose_view.alpha = 0.0
                
            }, completion: nil)
        }
        else
        {
            //Print("lose cancel btn")
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.lose_view.isHidden = true
                self.lose_view.alpha = 0.0
                
            }, completion: nil)
        }
        
        
        if self.audioPlayer_gameOver.isPlaying
        {
            self.audioPlayer_gameOver.stop()
        }
    
        
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKind(of: PlayGameViewController.self)
            {
                self.navigationController?.popToViewController(controller , animated: true)
                break
            }
        }
        
        
    }
    
    

    
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //Print("getPlayerDict.count = \(resultArr.count)")
        return MeArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! PlayerNameCollectionViewCell
        
             //Print("resultArr[indexPath.row] as? String = \(MeArray[indexPath.row] as? String)")
        
                let strScore = MeArray[indexPath.row] as? String
                let strName = MeArray[indexPath.row] as? String
                let UserName = strName?.components(separatedBy: ":")[0]
                

                let tempStr = strScore?.components(separatedBy: ":")[1]
                let TotalScore_user = (tempStr?.components(separatedBy: " ")[0])!
                cell.playerName_lbl.text = UserName?.uppercased()
                cell.totalScore_lbl.text = TotalScore_user
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        //Print("You selected cell #\(indexPath.item)!")
    }
    
    
    
    
     // MARK: - Show Interstitial AdsOn Completion
    func showInterstitialAdsOnCompletion()
    {
        if (UserDefaults.standard.value(forKey: "displayAds_value_gameCompleted") != nil)
        {
            var displayAds_value_gameCompleted = Int()
            displayAds_value_gameCompleted = UserDefaults.standard.value(forKey: "displayAds_value_gameCompleted") as! Int
            print("displayAds_value_gameCompleted before increment = \(displayAds_value_gameCompleted)")
            
            displayAds_value_gameCompleted = displayAds_value_gameCompleted + 1
            UserDefaults.standard.set(displayAds_value_gameCompleted, forKey: "displayAds_value_gameCompleted")
            print("displayAds_value_gameCompleted after increment = \(displayAds_value_gameCompleted)")
            
            if displayAds_value_gameCompleted % 3 == 0
            {
                // Display the intertitial ad
                interstitialAd = createAndLoadInterstitial_OnCompletesGame()
            }
            else
            {
                print("display ads variable is not the multiple of 3")
            }
            
        }
    }
    
    
    
    
    
    
    // MARK: - Show Interstitial AdsOn Game Quit
    func showInterstitialAdsOnGameQuit()
    {
        // Display the intertitial ad
        interstitialAd = createAndLoadInterstitial_OnUserQuits()
    }

    
    
    
    
    
    
    // MARK: - Help methods to Load Interestitial Ads
    func createAndLoadInterstitial_OnUserQuits() -> GADInterstitial? {
        //        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-8501671653071605/2568258533") //Test ID
        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-3259941691864510/8857053184") //Client ID
        
        guard let interstitial = interstitialAd else {
            return nil
        }
        
        let request = GADRequest()
//        request.testDevices = [ kGADSimulatorID,"5c8891a4a24b9906205ad319f72f9ffe" ]
        interstitial.load(request)
        interstitial.delegate = self
        
        return interstitial
    }
    
    
    // MARK: - Help methods to Load Interestitial Ads
    func createAndLoadInterstitial_OnCompletesGame() -> GADInterstitial? {
        //        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-8501671653071605/2568258533") //Test ID
        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-3259941691864510/2810519588") //Client ID
        
        guard let interstitial = interstitialAd else {
            return nil
        }
        
        let request = GADRequest()
//        request.testDevices = [ kGADSimulatorID,"5c8891a4a24b9906205ad319f72f9ffe" ]
        interstitial.load(request)
        interstitial.delegate = self
        
        return interstitial
    }
    
    
    //MARK:- INSTERSTITIAL ADS DELEGATES
    
    /// Called when an interstitial ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial!)
    {
        print("interstitialDidReceiveAd")
        interstitialAd!.present(fromRootViewController: self)
    }
    
    
    /// Called when an interstitial ad request failed.
    func interstitial(_ ad: GADInterstitial!, didFailToReceiveAdWithError error: GADRequestError!)
    {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Called just before presenting an interstitial.
    func interstitialWillPresentScreen(_ ad: GADInterstitial!)
    {
        print("interstitialWillPresentScreen")
    }
    
    /// Called before the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial!)
    {
        print("interstitialWillDismissScreen")
        gameQuitAds_bool = true
    }
    
    
    /// Called just before the application will background or terminate because the user clicked on an
    /// ad that will launch another app (such as the App Store).
    func interstitialWillLeaveApplication(_ ad: GADInterstitial!)
    {
        print("interstitialWillLeaveApplication")
    }
    

    
    
    
    // MARK: - Did-Receive-Memory-Warning Method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
   

}
