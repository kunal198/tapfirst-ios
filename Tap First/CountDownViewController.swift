//
//  CountDownViewController.swift
//  Tap First
//
//  Created by brst on 27/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
import GoogleMobileAds

class CountDownViewController: UIViewController,UIAlertViewDelegate,GADInterstitialDelegate {

    // MARK: - Variables Declaration
    var audioCountDown:AVAudioPlayer!
    var audioPlayer_gameOver:AVAudioPlayer!
    var playerSelectionString = String()
    var count = 3
    var pairUserArray = NSMutableArray()
    var getUserBool = Bool()
    var strusercount = NSString()
    var userStatus = NSString()
    var nameStr = String()
    var timer = Timer()
    var checkPush = String()
    var testArray = [String]()
    var interstitialAd : GADInterstitial!
    var getNetworkStatus = String()
    var  usercount = String()
    var getNetworkCount = Int()
    
    
    // MARK: - Outlets Connection
    @IBOutlet var loadingView: UIView!
    @IBOutlet var wrapperView: UIView!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var circleImage: UIImageView!
    @IBOutlet var counts_lbl: UILabel!
    @IBOutlet var playLbl: UILabel!
    @IBOutlet var closeViewBtn: UIButton!
    @IBOutlet var retryBtn: UIButton!
    @IBOutlet var LoseView: UIView!
    
    
    
     // MARK: - View-Did-Load Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        getNetworkCount = 0
        
        NotificationCenter.default.addObserver(self, selector: #selector(CountDownViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
//        Reach().monitorReachabilityChanges()
        
        let status = Reach().connectionStatus()
        switch status
        {
        case .unknown, .offline:
            print("Not connected on countdown screen")
            
        case .online(.wwan):
            print("Connected via WWAN on count down screen")
            
            usersGroup()
            loadingView.isHidden = false
            wrapperView.isHidden = false
            
            print(" self.playerSelectionStringon countDownScreen = \( self.playerSelectionString)")
            
            backgroundBoolForCountDown = false
            
        case .online(.wiFi):
            print("Connected via WiFi on count down screen")
            
            usersGroup()
            
            loadingView.isHidden = false
            wrapperView.isHidden = false
            
            print(" self.playerSelectionStringon countDownScreen = \( self.playerSelectionString)")
            
            backgroundBoolForCountDown = false
        }
        
        
    }
    
    
     // MARK: - View-Did-DisAppear Method
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    // MARK: - Get Network Status Reachibility Method
    func networkStatusChanged(_ notification: Notification)
    {
        
        let userInfo = (notification as NSNotification).userInfo
        print("userInfo for network reachibility = \(userInfo)")
        
        getNetworkStatus = (userInfo?["Status"] as? String)!
        print("userInfo for network reachibility in string = \(getNetworkStatus)")
        
        if getNetworkStatus == "Offline"
        {
            loadingView.isHidden = true
            wrapperView.isHidden = true
            
            let alert = UIAlertController(title: "", message: "No Internet Connection. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                print("You've pressed OK button");

                NotificationCenter.default.removeObserver(self)
                
                let childRef = FIRDatabase.database().reference()
                if self.usercount == "1"
                {
                    if let stringArray = UserDefaults.standard.value(forKey: "keyGenerated") as? String
                    {
                        childRef.child("groups").child(UserDefaults.standard.value(forKey: "playerSelectionString") as! String).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).setValue(nil)
                    }
                    
                    UserDefaults.standard.set(nil, forKey: "NewGroupKeyGenerated")
                    UserDefaults.standard.set(nil, forKey: "keyGenerated")
                }
                
                for controller in self.navigationController!.viewControllers as Array
                {
                    if controller.isKind(of: PlayGameViewController.self)
                    {
                        self.navigationController?.popToViewController(controller , animated: true)
                        break
                    }
                }

                
                
            }
            
            alert.addAction(OKAction)
            
            if presentedViewController == nil
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.dismiss(animated: false) { () -> Void in
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }
    }
    
  
    
    
    
    // MARK: -  Call Foreground Method
    func cameIntoForeground ()
    {
        if self.timer.isValid == true
        {
            self.timer.invalidate()
            print("timer invalidated!!!!")
        }
        
        moveToPlayingScreenBool = true
        
        if audioCountDown.isPlaying
        {
            audioCountDown.stop()
        }
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            
            self.LoseView.isHidden = false
            self.LoseView.alpha = 1.0
            
        }, completion: nil)

        PlaySoundOnLose()
        
        self.createAndLoadInterstitial()
    }
    
    
    // MARK: - Play Lost Audio Method
    func PlaySoundOnLose()
    {
        let url = Bundle.main.url(forResource: "Lose", withExtension: ".mp3")!
        do
        {
            audioPlayer_gameOver = try AVAudioPlayer(contentsOf: url)
            guard let player = audioPlayer_gameOver else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            //Print(error.localizedDescription)
        }
        
    }
    
    
    // MARK: -   Checking Users Group In the Firebase
    func usersGroup()
    {
        
         let childRef = FIRDatabase.database().reference()
       
        childRef.child("groups").child(playerSelectionString).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).child("users").observe(FIRDataEventType.childAdded, with: { snapshot in
            
            print("snapshot for users childAdded = \(snapshot)")
            
            childRef.child("groups").child(self.playerSelectionString).child(UserDefaults.standard.value(forKey: "keyGenerated") as! String).observeSingleEvent(of: FIRDataEventType.value, with: { snapshot in
                
                print("snapshot for users childChanged = \(snapshot)")
                
                let id = snapshot.value as? NSDictionary
                
                self.usercount = (id?.value(forKey: "user_count") as! NSString) as String
                
                let strUser = id!.value(forKey: "users") as! NSDictionary
                print("strUser = \(strUser)")
                
                if self.playerSelectionString == "two_users"
                {
                    if self.usercount == "2"
                    {
                        if self.pairUserArray.count > 0
                        {
                            self.pairUserArray.removeAllObjects()
                        }
                        
                        self.pairUserArray.add(strUser)
                        print("self.pairUserArray in countDown = \(self.pairUserArray)")
                        
                        UserDefaults.standard.set(self.pairUserArray, forKey: "CurrentUsersArray")
                        UserDefaults.standard.set(false, forKey: "WaitingForOpponent")
                        
                        if audioPlayer != nil
                        {
                            if audioPlayer.isPlaying
                            {
                                audioPlayer.stop()
                            }
                        }
                        
                        self.hiddenViews()
                        
                        childRef.removeAllObservers()
                    }
                }
                else if  self.playerSelectionString == "three_users"
                {
                    if self.usercount == "3"
                    {
                        
                        if self.pairUserArray.count > 0
                        {
                            self.pairUserArray.removeAllObjects()
                        }
                        
                        self.pairUserArray.add(strUser)
                        print("self.pairUserArray in countDown = \(self.pairUserArray)")
                        
                        UserDefaults.standard.set(self.pairUserArray, forKey: "CurrentUsersArray")
                        UserDefaults.standard.set(false, forKey: "WaitingForOpponent")
                        
                        if audioPlayer != nil
                        {
                            if audioPlayer.isPlaying
                            {
                                audioPlayer.stop()
                            }
                        }
                        
                        
                        self.hiddenViews()
                        childRef.removeAllObservers()
                    }
                }
                else if  self.playerSelectionString == "four_users"
                {
                    if self.usercount == "4"
                    {
                        
                        if self.pairUserArray.count > 0
                        {
                            self.pairUserArray.removeAllObjects()
                        }
                        
                        self.pairUserArray.add(strUser)
                        print("self.pairUserArray in countDown = \(self.pairUserArray)")
                        
                        UserDefaults.standard.set(self.pairUserArray, forKey: "CurrentUsersArray")
                        UserDefaults.standard.set(false, forKey: "WaitingForOpponent")
                        
                        if audioPlayer != nil
                        {
                            if audioPlayer.isPlaying
                            {
                                audioPlayer.stop()
                            }
                        }
                        
                        
                        self.hiddenViews()
                        childRef.removeAllObservers()
                    }
                }
                else if  self.playerSelectionString == "five_users"
                {
                    if self.usercount == "5"
                    {
                        
                        if self.pairUserArray.count > 0
                        {
                            self.pairUserArray.removeAllObjects()
                        }
                        
                        self.pairUserArray.add(strUser)
                        print("self.pairUserArray in countDown = \(self.pairUserArray)")
                        
                        UserDefaults.standard.set(self.pairUserArray, forKey: "CurrentUsersArray")
                        UserDefaults.standard.set(false, forKey: "WaitingForOpponent")
                        
                        if audioPlayer != nil
                        {
                            if audioPlayer.isPlaying
                            {
                                audioPlayer.stop()
                            }
                        }
                        
                        self.hiddenViews()
                        childRef.removeAllObservers()
                    }
                }
                else if  self.playerSelectionString == "six_users"
                {
                    if self.usercount == "6"
                    {
                        
                        if self.pairUserArray.count > 0
                        {
                            self.pairUserArray.removeAllObjects()
                        }
                        
                        self.pairUserArray.add(strUser)
                        print("self.pairUserArray in countDown = \(self.pairUserArray)")
                        
                        UserDefaults.standard.set(self.pairUserArray, forKey: "CurrentUsersArray")
                        UserDefaults.standard.set(false, forKey: "WaitingForOpponent")
                        
                        if audioPlayer != nil
                        {
                            if audioPlayer.isPlaying
                            {
                                audioPlayer.stop()
                            }
                        }
                        
                        self.hiddenViews()
                        childRef.removeAllObservers()
                    }
                }
                
      
                
            })
            
        })

    }
    
    
    
    // MARK: -  NSNotification Method
    func methodOfReceivedNotification(notification: NSNotification)
    {
        print("receive nsnotification for all posts listing")
    }
    
    
    
    // MARK: - View-Will-Appear Method
    override func viewWillAppear(_ animated: Bool)
    {
    
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(cameIntoForeground),
                                               name: NSNotification.Name.UIApplicationWillEnterForeground,
                                               object: nil)
    }
    
    
    // MARK: - Call App BAckground Notification Method
    func goToBackground()
    {
        backgroundBoolForCountDown = true
    }
    
    
    
     // MARK: -  Remove Duplicate Array Value
    func removeDuplicates(array: [String]) -> [String]
    {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array
        {
            if encountered.contains(value)
            {
                // Do not add a duplicate element.
            }
            else
            {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }

    
     // MARK: -  Play Count-Down Audio File Method
    func PlaySoundInBackground()
    {
        let url = Bundle.main.url(forResource: "1, 2, 3, Play ", withExtension: ".mp3")!
        do
        {
            audioCountDown = try AVAudioPlayer(contentsOf: url)
            guard let player = audioCountDown else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }

    
    // MARK: -  Game Over Lost Alert Method
    @IBAction func GameOver_Action(_ sender: UIButton)
    {
        if self.audioPlayer_gameOver.isPlaying
        {
            self.audioPlayer_gameOver.stop()
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userLost"), object: nil)

        
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKind(of: PlayGameViewController.self)
            {
                self.navigationController?.popToViewController(controller , animated: true)
                break
            }
        }
    }
    

    
    // MARK: -  Unhide Count-Down View
    func hiddenViews()
    {
        self.loadingView.isHidden = true
        self.wrapperView.isHidden = true
        
        if self.timer.isValid == true
        {
            self.timer.invalidate()
        }
        
        initializeUserDefaultsValues()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(CountDownViewController.countdown), userInfo: nil, repeats: true)
        PlaySoundInBackground()
    }
    
    
    // MARK: - initializeUserDefaultsValues()
    func initializeUserDefaultsValues()
    {
        if (UserDefaults.standard.value(forKey: "totalWin_score") == nil)
        {
            UserDefaults.standard.set(0, forKey: "totalWin_score")
        }
        
        if (UserDefaults.standard.value(forKey: "totalLose_score") == nil)
        {
            UserDefaults.standard.set(0, forKey: "totalLose_score")
        }
        
    }
    
    
     // MARK: -  Count-Down Method Begins
    func countdown()
    {

        if(count > 0)
        {
            count = count - 1
            
            if count == 2
            {
                if (UserDefaults.standard.value(forKey: "totalGamesPlayed") == nil)
                {
                    UserDefaults.standard.set(1, forKey: "totalGamesPlayed")
                }
                else
                {
                    var totalPlayed_score = Int()
                    totalPlayed_score = UserDefaults.standard.value(forKey: "totalGamesPlayed") as! Int
                    //Print("total played score before increment = \(totalPlayed_score)")
                    
                    totalPlayed_score = totalPlayed_score + 1
                    //Print("total played score after increment = \(totalPlayed_score)")
                    
                    UserDefaults.standard.set(totalPlayed_score, forKey: "totalGamesPlayed")
                }
                
            }

            
            if count == 0
            {
                playLbl.isHidden=false
                counts_lbl.isHidden=true
                PlaySoundInBackground()
//                pushMethod()
                moveToPlayingScreenBool = false
                self.perform(#selector(CountDownViewController.pushMethod), with: nil, afterDelay: 1.0)
              
            }
            else
            {
                playLbl.isHidden=true
                counts_lbl.isHidden=false
                counts_lbl.text = String(count)
                PlaySoundInBackground()
            }
        }
        
        
    }
    
    // MARK: -  Move to Game Playing Screen Method
    func pushMethod()
    {
        if moveToPlayingScreenBool == false
        {
            let startPlayingScreen = self.storyboard?.instantiateViewController(withIdentifier: "StartPlayingGameViewController") as! StartPlayingGameViewController
            //        print(self.pairUserArray)
            //        startPlayingScreen.pairUserArray = self.pairUserArray
            
            if audioCountDown.isPlaying
            {
                audioCountDown.stop()
            }
            
            startPlayingScreen.playerSelectionString = self.playerSelectionString
            self.navigationController?.pushViewController(startPlayingScreen, animated: true)
        }
    }

    
    // MARK: -  Back Btn Method
    @IBAction func backBtn_Action(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    // MARK: - Help methods to Load Interestitial Ads
    func createAndLoadInterstitial() -> GADInterstitial? {
//        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-8501671653071605/2568258533") //Test ID
        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-3259941691864510/8857053184") //Client ID
        guard let interstitial = interstitialAd else {
            return nil
        }
        
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID,"5c8891a4a24b9906205ad319f72f9ffe" ]
        interstitial.load(request)
        interstitial.delegate = self
        
        return interstitial
    }

    
    
    //MARK:- INSTERSTITIAL ADS DELEGATES
    
    /// Called when an interstitial ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial!)
    {
        print("interstitialDidReceiveAd")
        interstitialAd!.present(fromRootViewController: self)
    }
    
    
    /// Called when an interstitial ad request failed.
    func interstitial(_ ad: GADInterstitial!, didFailToReceiveAdWithError error: GADRequestError!)
    {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Called just before presenting an interstitial.
    func interstitialWillPresentScreen(_ ad: GADInterstitial!)
    {
        print("interstitialWillPresentScreen")
    }
    
    /// Called before the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial!)
    {
        print("interstitialWillDismissScreen")
    }
    
    
    /// Called just before the application will background or terminate because the user clicked on an
    /// ad that will launch another app (such as the App Store).
    func interstitialWillLeaveApplication(_ ad: GADInterstitial!)
    {
        print("interstitialWillLeaveApplication")
    }

    
    
    
    // MARK: -  Did-Receive-Memory-Warning Method
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  

}
