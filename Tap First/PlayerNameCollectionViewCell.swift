//
//  PlayerNameCollectionViewCell.swift
//  Tap First
//
//  Created by brst on 28/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class PlayerNameCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var playerName_lbl: UILabel!
    @IBOutlet var totalScore_lbl: UILabel!
    
}
