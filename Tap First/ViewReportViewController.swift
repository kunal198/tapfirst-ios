//
//  ViewReportViewController.swift
//  Tap First
//
//  Created by brst on 27/12/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import AVFoundation

class ViewReportViewController: UIViewController
{
    // MARK: - Variables Declaration
    var totalPlayed_value = Int()
    var totalWin_value = Int()
    var totalLose_value = Int()
    var audioPlayer:AVAudioPlayer!
    
    
    // MARK: - Outlets Connection
    @IBOutlet var totalPlayed_valueLbl: UILabel!
    @IBOutlet var totalWon_valueLbl: UILabel!
    @IBOutlet var totalLose_valueLbl: UILabel!
    
    // MARK: - View-Did-Load Method
    override func viewDidLoad()
    {
        super.viewDidLoad()

        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.isNavigationBarHidden = true
    }

    // MARK: - View-Will-Appear Method
    override func viewWillAppear(_ animated: Bool)
    {
    
          if (UserDefaults.standard.value(forKey: "totalWin_score") != nil)
          {
             totalWin_value = UserDefaults.standard.value(forKey: "totalWin_score") as! Int
             totalWon_valueLbl.text = String(totalWin_value)
          }
          else
         {
            totalWin_value = 0
            totalWon_valueLbl.text = String(totalWin_value)
          }
        
        
        if (UserDefaults.standard.value(forKey: "totalGamesPlayed") != nil)
        {
            totalPlayed_value = UserDefaults.standard.value(forKey: "totalGamesPlayed") as! Int
            totalPlayed_valueLbl.text = String(totalPlayed_value)
        }
        else
        {
            totalPlayed_value = 0
            totalPlayed_valueLbl.text = String(totalPlayed_value)
        }
        
        totalLose_value = totalPlayed_value - totalWin_value
        totalLose_valueLbl.text = String(totalLose_value)
        
    }
    
    
    
    // MARK: - Play Audio File In Background Method
    func PlaySoundInBackground()
    {
        
        let audioFilePath = Bundle.main.path(forResource: "Playscreen, enter username screen, choose how many players screen and Match report screen. ", ofType: "mp3")
        
        let url = URL(fileURLWithPath: audioFilePath!)
        
        do
        {
            let sound = try AVAudioPlayer(contentsOf: url)
            audioPlayer = sound
            sound.play()
        }
        catch
        {
            // couldn't load file :(
            print("couldn't load file")
        }
        
    }

    
    
    // MARK: - Back Btn Method
    @IBAction func backBtn_Action(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Did-Receive-Memory-Warning Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
